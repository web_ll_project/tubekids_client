import { Component, OnInit } from '@angular/core';
import { AuthenticationService, ChildDetails } from '../authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {

  constructor(private auth: AuthenticationService, private router: Router) { }
  details: ChildDetails;
  ngOnInit(): void {
    /*
    this.auth.child().subscribe(
      child => {
        this.details = child
      },
      err => {
        console.error(err)
      }
    )*/
  }
  
  registerChild(){

  }

}
