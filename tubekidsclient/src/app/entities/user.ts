export class User {

    constructor(
        public _id: number,
        public firstname: string,
        public lastname: string,
        public email: string,
        public phone: number,
        public borndate: Date,
        public country: string,
        public password: string,
        public code: string,
        public isVerified: boolean
    ){}
}
