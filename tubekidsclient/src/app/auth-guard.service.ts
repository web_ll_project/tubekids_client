import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService  implements CanActivate {

  constructor(private auth: AuthenticationService, private router: Router) {}
  
  canActivate(){
    if(!this.auth.isLoggedIn()) {
      this.router.navigateByUrl('/login')
      return false
    } 
    /*else if(this.auth.isLoggedIn() && !this.auth.isVery()){
      this.router.navigateByUrl('/send')
    }*/
    return true
  }
  
}
