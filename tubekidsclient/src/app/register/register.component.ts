import { Component, OnInit } from '@angular/core';
import { AuthenticationService, TokenPayload } from '../authentication.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  credentials: TokenPayload = {
    _id: '',
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    phone: 0,
    country: '',
    code: '',
    isVerified: false,
    bornDate: new Date()
  }
  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
  }
  register() {
    this.auth.register(this.credentials).subscribe(
      () => {
        this.router.navigateByUrl('/send')
      },
      err => {
        console.error(err)
      }
    )
  }

}
