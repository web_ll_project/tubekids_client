import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import {DatePipe} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

    private token: string

    constructor(private http: HttpClient, private router: Router,private datepipe: DatePipe){}
    private saveToken(token: string): void{
      localStorage.setItem('usertoken',token)
      this.token=token;
    }

    private getToken(): string {
      if(this.token){
        this.token = localStorage.getItem('usertoken')
      }
      return this.token;
    }
    
    public getUserDetails(): UserDetails {
      const token = this.getToken()
      let payload
      if(token) {
        payload = token.split('.')[1]
        payload = window.atob(payload)
        return JSON.parse(payload)
      }else{
        return null
      }
    }

    public isLoggedIn(): boolean {
      const user = this.getUserDetails()
      if(user){
        return user.exp > Date.now() / 1000
      }else{
        return false
      }
    }

    public isVery(): boolean {
      const user = this.getUserDetails()
      if(user.isVerified){
        return true
      }else{
        return false
      }
    }

    public register(user: TokenPayload): Observable<any>{
      this.datepipe.transform(user.bornDate, 'yyyy-MM-dd');
      const base = this.http.post('http://localhost:3000/users/register',user)
      const request = base.pipe(
        map((data: TokenResponse)=> {
          if(data.token){
            this.saveToken(data.token)
          }
          return data
        })
      )
      return request
    }

    public login(user: TokenPayload): Observable<any> {
      const base = this.http.post('http://localhost:3000/users/login',user);

      const request = base.pipe(
        map((data: TokenResponse)=> {
          if(data.token){
            this.saveToken(data.token)
          }
          return data
        })
      )
      return request
    }

    public profile(): Observable<any>{
      return this.http.get('http://localhost:3000/users/profile', {
        headers: {Authorization: `${this.getToken()}`,
      }
      })
    }
    public child(): Observable<any>{
      return this.http.get('http://localhost:3000/users/child', {
        headers: {Authorization: `${this.getToken()}`,
      }
      })
    }

    public send(): Observable<any>{
      return this.http.get('http://localhost:3000/users/send', {
        headers: {Authorization: `${this.getToken()}`}
      })
    }

    public verifyAccount(): Observable<any>{
      return this.http.put('http://localhost:3000/users/verifyAccount', {
        headers: {Authorization: `${this.getToken()}`}
      })
    }
    public update(): Observable<any>{
      return this.http.put('http://localhost:3000/users/update', {
        headers: {Authorization: `${this.getToken()}`}
      })
    }

    public logout (): void {
      this.token =''
      window.localStorage.removeItem('usertoken')
      this.router.navigateByUrl('/login')
    }
}

export interface UserDetails{
    _id: string,
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    phone: number,
    country: string,
    bornDate: Date,
    code: string,
    isVerified: boolean,
    exp: number,
    iat: number

}
export interface ChildDetails{
  _id: string,
  email: string,
  password: string,
  username: string
}

interface TokenResponse {
  token: string
}

export interface TokenPayload {
    _id: string,
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    phone: number,
    country: string,
    bornDate: Date,
    code:string,
    isVerified: boolean
}
