import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildboardComponent } from './childboard.component';

describe('ChildboardComponent', () => {
  let component: ChildboardComponent;
  let fixture: ComponentFixture<ChildboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
