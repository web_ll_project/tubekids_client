import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {DatePipe} from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthenticationService } from './authentication.service';
import { AuthGuardService } from './auth-guard.service';
import { SendComponent } from './send/send.component';
import { ChildComponent } from './child/child.component';
import { PlaylistComponent } from './playlist/playlist.component';
import { ChildboardComponent } from './childboard/childboard.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuardService]},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService]},
  {path: 'send', component: SendComponent, canActivate: [AuthGuardService]},
  {path: 'verifyAccount', component: SendComponent, canActivate: [AuthGuardService]},
  {path: 'child', component: ChildComponent, canActivate: [AuthGuardService]},
  {path: 'playlist', component: PlaylistComponent, canActivate: [AuthGuardService]},


]

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    SendComponent,
    ChildComponent,
    PlaylistComponent,
    ChildboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [AuthenticationService, AuthGuardService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
