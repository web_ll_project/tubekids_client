import { Component, OnInit } from '@angular/core';
import { AuthenticationService, TokenPayload } from '../authentication.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  credentials: TokenPayload = {
    _id: '',
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    phone: 0,
    country: '',
    code:'',
    isVerified: false,
    bornDate: new Date()
  }
  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
  }
  login() {
    this.auth.login(this.credentials).subscribe(
      () => {
        this.router.navigateByUrl('/home')
      },
      err => {
        console.error(err)
      }
    )
  }
}
